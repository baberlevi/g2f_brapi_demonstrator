from django.contrib import admin
from brapi.models import *
from django.apps import apps
#from rest_framework_cache.registry import cache_registry
#cache_registry.autodiscover()

# Register your models here.
for model in apps.get_app_config('brapi').models.values():
    admin.site.register(model)